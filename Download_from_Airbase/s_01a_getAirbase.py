# runs under ClimPol02

import urllib.request
from datetime import date, timedelta
import os

###############################################################################
# variables for this script
# years: from and to
startY = '2016'
endY = '2020'
# pollutants and averaging time:
# 38=NO
# 8=NO2
# 7=O3
# 5=PM10
# 6001=PM2.5

# Pollutants to download:
pollutantCodes = ['8']
#pollutantCodes = ['5', '38', '8', '7']
#pollutantCodes = ['6001']
#pollutantCodes = ['5', '38', '8', '7', '6001']
#pollutantCodes = [
#'428',
#'503',
#'394',
#'486',
#'443',
#'430',
#'6006',
#'6007',
#'451',
#'432',
#'20',
#'21',
#'464',
#'482',
#'6013',
#'6012',
#'6011',
#'431'
#]
# Country:
# DE=Germany
countries=['DE']
#countries=['']
# Cities:
cities=['']
# Stations:
stations=['DEBE034', 'DEBE065']
samplingPoint=''
source='E1a'
#source='E2a'
timeCoverage='YEAR'
###############################################################################

###############################################################################
# dicts and aux data for the script
pollutantNames = {
     '38': 'NO',
     '8': 'NO2',
     '7': 'O3',
     '5': 'PM10',
     '6001': 'PM2.5'
 }
#pollutantNames = {
#'428':	'Ethane',
#'503':	'Propane',
#'394':	'Butane',
#'486':	'Pentane',
#'443':	'Hexane',
#'430':	'Ethene',
#'6006':	'trans-2-Butene',
#'6007':	'cis-2-Butene',
#'451':	'Isoprene',
#'432':	'Ethyne',
#'20':	'Benzene',
#'21':	'Toluene',
#'464':	'm_p-Xylene',
#'482':	'o-Xylene',
#'6013':	'1,3,5-Trimethylbenzene',
#'6012':	'1,2,3-Trimethylbenzene',
#'6011':	'1,2,4-Trimethylbenzene',
#'431':	'Ethylbenzene'
#}
###############################################################################

###############################################################################
# make the dir where to keep the downloaded files
dirToKeep = date.today().strftime("%Y-%m-%d")+'__downloaded'
if not os.path.exists(dirToKeep):
    os.mkdir(dirToKeep)
###############################################################################

###############################################################################
def getFilesList(countryCode, cityName, polCode, yearFrom, yearTo, stationCode, samplingPointCode, sourceE, timeCoverageT) :

    url = 'https://fme.discomap.eea.europa.eu/fmedatastreaming/AirQualityDownload/AQData_Extract.fmw?'
    url += 'CountryCode=' + countryCode
    url += '&CityName=' + cityName
    url += '&Pollutant=' + polCode
    url += '&Year_from=' + yearFrom
    url += '&Year_to=' + yearTo
    url += '&Station=' + stationCode
    url += '&Samplingpoint=' + samplingPointCode
    url += '&Source=' + sourceE
    url += '&Output=TEXT&UpdateDate='
    url += '&TimeCoverage=' + timeCoverageT

    # download the page and save the result:
    response = urllib.request.urlopen(url)
    webContent = response.read()
    filename = dirToKeep + '/' + pollutantNames[polCode] + '__' + yearFrom + '-' + yearTo + '_' + countryCode + '.csv'
    f = open(filename, 'wb')
    f.write(webContent)
    f.close
    return filename

###############################################################################


if __name__ == "__main__" :

    for pollutantCode in pollutantCodes :
        for country in countries :
            for city in cities :
                for station in stations :
                    # first get the list of the files to download
                    urls=getFilesList(country, city, pollutantCode, startY, endY, station, samplingPoint, source, timeCoverage)
                    # download the files using wget
                    os.chdir(dirToKeep)
                    cmd = "wget -i " + urls.rsplit('/', 1)[-1]
                    os.system(cmd)
                    os.chdir('../')
