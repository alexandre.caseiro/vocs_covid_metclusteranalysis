Number of valid subsets (SMP and wd/we combinations for all stations):
110
Means of Ratio of Means:
All subsets:  0.9944445 pm 0.1721637
   c_cluster c_wdwe c_meanCLoverBY
1          2     wd      1.1370105
2          3     wd      1.0034199
3          4     wd      0.9928571
4          5     wd      1.0477384
5          6     wd      0.9295710
6          7     wd      0.9940302
7          8     wd      0.9657740
8          2     we      0.9800000
9          4     we      0.9457143
10         5     we      0.9518428
11         6     we      1.0000000
12         7     we      0.9515789
13         8     we      1.0015705

Medians of Ratio of Means:
All subsets:  1
   c_cluster c_wdwe c_meanCLoverBY
1          2     wd              1
2          3     wd              1
3          4     wd              1
4          5     wd              1
5          6     wd              1
6          7     wd              1
7          8     wd              1
8          2     we              1
9          4     we              1
10         5     we              1
11         6     we              1
12         7     we              1
13         8     we              1

Means of p-values:
All subsets:  0.6385107 pm 0.2747307
   c_cluster c_wdwe    c_pval
1          2     wd 0.5928755
2          3     wd 0.8186759
3          4     wd 0.8136637
4          5     wd 0.5122326
5          6     wd 0.2154245
6          7     wd 0.5501531
7          8     wd 0.6579437
8          2     we 1.0000000
9          4     we 0.4055030
10         5     we 0.5674937
11         7     we 0.6453877
12         8     we 1.0000000

Medians of p-values:
All subsets:  0.6373519
   c_cluster c_wdwe    c_pval
1          2     wd 0.6170751
2          3     wd 0.8186759
3          4     wd 0.8136637
4          5     wd 0.5806529
5          6     wd 0.2154245
6          7     wd 0.5438379
7          8     wd 0.8914788
8          2     we 1.0000000
9          4     we 0.4055030
10         5     we 0.5674937
11         7     we 0.6453877
12         8     we 1.0000000

Means of Ratio of Medians:
All subsets:  1.033049 pm 0.3927295
   c_cluster c_wdwe c_medianCLoverBY
1          2     wd        1.4961039
2          3     wd        0.9750000
3          4     wd        1.0000000
4          5     wd        1.0222222
5          6     wd        0.9625000
6          7     wd        1.0000000
7          8     wd        1.0000000
8          2     we        0.9800000
9          4     we        0.9457143
10         5     we        0.9720000
11         6     we        1.0000000
12         7     we        1.0000000
13         8     we        1.0000000

Medians of Ratio of Medians:
All subsets:  1
   c_cluster c_wdwe c_medianCLoverBY
1          2     wd                1
2          3     wd                1
3          4     wd                1
4          5     wd                1
5          6     wd                1
6          7     wd                1
7          8     wd                1
8          2     we                1
9          4     we                1
10         5     we                1
11         6     we                1
12         7     we                1
13         8     we                1

Number of traffic, industrial and background stations and subsets:
[1] 0 0
[1]  2 22
[1]  8 88


Number of urban, suburban and rural stations and subsets:
[1]  1 11
[1]  4 44
[1]  5 55


All stations:
WD/WE Cluster Avg_Ld sd_Ld Avg_noLd sd_noLd frac_RoA<1 RoA_median median_reduc_frac avg_reduc_frac n 
wd 1 NaN NA 0.0979 0.03164465 NA NA NA NaN 0 
wd 2 0.09 0 0.08766667 0.02582228 78 1 0 -0.1370105 10 
wd 3 0.09266667 0.00843274 0.09356667 0.0150858 78 1 0 -0.003419906 10 
wd 4 0.09 0 0.09033333 0.001054093 NA 1 0 0.007142857 5 
wd 5 0.098 0.02529822 0.09296032 0.01132597 67 1 0 -0.04773839 10 
wd 6 0.09166667 0.005270463 0.105 0.04046919 NA 1 0 0.07042903 10 
wd 7 0.09 0 0.09117483 0.008172735 89 1 0 0.005969837 10 
wd 8 0.0921 0.00360401 0.09646405 0.01363933 89 1 0 0.03422601 10 

 weighted average (median_reduc_frac) : 0 ( 0 - 0 ) 
 weighted average (avg_reduc_frac) : -0.01138038 ( -0.1370105 - 0.07042903 ) 
 
we 1 NaN NA 0.09333333 0.01054093 NA NA NA NaN 0 
we 2 0.09 0 0.091 0.003162278 NA 1 0 0.02 5 
we 3 NaN NA 0.076875 0.02604083 NA NA NA NaN 0 
we 4 0.09 0 0.0975 0.01903943 NA 1 0 0.05428571 10 
we 5 0.09 0 0.09775 0.0236423 NA 1 0 0.04815725 10 
we 6 0.09 0 0.09 0 NA 1 0 0 5 
we 7 0.09 0 0.092875 0.009091548 NA 1 0 0.04842105 5 
we 8 0.09 0 0.09011905 0.004940118 89 1 0 -0.001570527 10 

 weighted average (median_reduc_frac) : 0 ( 0 - 0 ) 
 weighted average (avg_reduc_frac) : 0.03001844 ( -0.001570527 - 0.05428571 ) 
 

Traffic stations:
WD/WE Cluster Avg_Ld sd_Ld Avg_noLd sd_noLd frac_RoA<1 RoA_median median_reduc_frac avg_reduc_frac n 
wd 1 NaN NA NaN NA NA NA NA NaN 0 
wd 2 NaN NA NaN NA NA NA NA NaN 0 
wd 3 NaN NA NaN NA NA NA NA NaN 0 
wd 4 NaN NA NaN NA NA NA NA NaN 0 
wd 5 NaN NA NaN NA NA NA NA NaN 0 
wd 6 NaN NA NaN NA NA NA NA NaN 0 
wd 7 NaN NA NaN NA NA NA NA NaN 0 
wd 8 NaN NA NaN NA NA NA NA NaN 0 

 weighted average (median_reduc_frac) : NaN ( Inf - -Inf ) 
 weighted average (avg_reduc_frac) : NaN ( Inf - -Inf ) 
 
we 1 NaN NA NaN NA NA NA NA NaN 0 
we 2 NaN NA NaN NA NA NA NA NaN 0 
we 3 NaN NA NaN NA NA NA NA NaN 0 
we 4 NaN NA NaN NA NA NA NA NaN 0 
we 5 NaN NA NaN NA NA NA NA NaN 0 
we 6 NaN NA NaN NA NA NA NA NaN 0 
we 7 NaN NA NaN NA NA NA NA NaN 0 
we 8 NaN NA NaN NA NA NA NA NaN 0 

 weighted average (median_reduc_frac) : NaN ( Inf - -Inf ) 
 weighted average (avg_reduc_frac) : NaN ( Inf - -Inf ) 
 

Industrial stations:
WD/WE Cluster Avg_Ld sd_Ld Avg_noLd sd_noLd frac_RoA<1 RoA_median median_reduc_frac avg_reduc_frac n 
wd 1 NaN NA 0.09166667 0.002357023 NA NA NA NaN 0 
wd 2 0.09 0 0.1116667 0.03064129 NA 0.8375 0.1625 0.1625 2 
wd 3 0.09 0 0.1025 0.01767767 NA 0.8913043 0.1086957 0.1086957 2 
wd 4 0.09 NA 0.09166667 0.002357023 NA 0.9642857 0.03571429 0.03571429 1 
wd 5 0.09 0 0.09888889 0.01257079 NA 0.9175258 0.08247423 0.08247423 2 
wd 6 0.09 0 0.1007143 0.01515229 NA 0.9038462 0.09615385 0.09615385 2 
wd 7 0.09 0 0.09269231 0.003807498 NA 0.9717742 0.02822581 0.02822581 2 
wd 8 0.093 0.004242641 0.1055556 0.02199888 NA 0.8963303 0.1036697 0.1036697 2 

 weighted average (median_reduc_frac) : 0.09224252 ( 0.02822581 - 0.1625 ) 
 weighted average (avg_reduc_frac) : 0.09224252 ( 0.02822581 - 0.1625 ) 
 
we 1 NaN NA 0.09 0 NA NA NA NaN 0 
we 2 0.09 NA 0.095 0.007071068 NA 0.9 0.1 0.1 1 
we 3 NaN NA 0.09 0 NA NA NA NaN 0 
we 4 0.09 0 0.12 0.04242641 NA 0.8 0.2 0.2 2 
we 5 0.09 0 0.09125 0.001767767 NA 0.9864865 0.01351351 0.01351351 2 
we 6 0.09 NA 0.09 0 NA 1 0 0 1 
we 7 0.09 NA 0.09 0 NA 1 0 0 1 
we 8 0.09 0 0.09285714 0.00404061 NA 0.9701493 0.02985075 0.02985075 2 

 weighted average (median_reduc_frac) : 0.06519206 ( 0 - 0.2 ) 
 weighted average (avg_reduc_frac) : 0.06519206 ( 0 - 0.2 ) 
 

Background stations:
WD/WE Cluster Avg_Ld sd_Ld Avg_noLd sd_noLd frac_RoA<1 RoA_median median_reduc_frac avg_reduc_frac n 
wd 1 NaN NA 0.09945833 0.03567665 NA NA NA NaN 0 
wd 2 0.09 0 0.08166667 0.02274775 72 1 0 -0.2118881 8 
wd 3 0.09333333 0.00942809 0.09133333 0.01481419 72 1 0 -0.0314488 8 
wd 4 0.09 0 0.09 0 NA 1 0 0 4 
wd 5 0.1 0.02828427 0.09147817 0.011393 58 1 0 -0.08029155 8 
wd 6 0.09208333 0.005892557 0.1060714 0.04545686 NA 1 0 0.06399782 8 
wd 7 0.09 0 0.09079545 0.009109559 86 1 0 0.0004058442 8 
wd 8 0.091875 0.003720119 0.09419118 0.01185444 86 1 0 0.01686508 8 

 weighted average (median_reduc_frac) : 0 ( 0 - 0 ) 
 weighted average (avg_reduc_frac) : -0.03728611 ( -0.2118881 - 0.06399782 ) 
 
we 1 NaN NA 0.09416667 0.01178511 NA NA NA NaN 0 
we 2 0.09 0 0.09 0 NA 1 0 0 4 
we 3 NaN NA 0.0725 0.0292831 NA NA NA NaN 0 
we 4 0.09 0 0.091875 0.005303301 NA 1 0 0.01785714 8 
we 5 0.09 0 0.099375 0.0265165 NA 1 0 0.05681818 8 
we 6 0.09 0 0.09 0 NA 1 0 0 4 
we 7 0.09 0 0.09359375 0.01016466 NA 1 0 0.06052632 4 
we 8 0.09 0 0.08943452 0.005134942 86 1 0 -0.009425845 8 

 weighted average (median_reduc_frac) : 0 ( 0 - 0 ) 
 weighted average (avg_reduc_frac) : 0.02122503 ( -0.009425845 - 0.06052632 ) 
 

Urban stations:
WD/WE Cluster Avg_Ld sd_Ld Avg_noLd sd_noLd frac_RoA<1 RoA_median median_reduc_frac avg_reduc_frac n 
wd 1 NaN NA 0.1866667 NA NA NA NA NaN 0 
wd 2 0.09 NA 0.11 NA NA 0.8181818 0.1818182 0.1818182 1 
wd 3 0.1166667 NA 0.125 NA NA 0.9333333 0.06666667 0.06666667 1 
wd 4 NaN NA 0.09 NA NA NA NA NaN 0 
wd 5 0.17 NA 0.1177778 NA 1 1.443396 -0.4433962 -0.4433962 1 
wd 6 0.1066667 NA 0.2185714 NA NA 0.4880174 0.5119826 0.5119826 1 
wd 7 0.09 NA 0.11 NA NA 0.8181818 0.1818182 0.1818182 1 
wd 8 0.1 NA 0.1235294 NA NA 0.8095238 0.1904762 0.1904762 1 

 weighted average (median_reduc_frac) : 0.1148943 ( -0.4433962 - 0.5119826 ) 
 weighted average (avg_reduc_frac) : 0.1148943 ( -0.4433962 - 0.5119826 ) 
 
we 1 NaN NA 0.1233333 NA NA NA NA NaN 0 
we 2 0.09 NA 0.09 NA NA 1 0 0 1 
we 3 NaN NA 0.09 NA NA NA NA NaN 0 
we 4 0.09 NA 0.105 NA NA 0.8571429 0.1428571 0.1428571 1 
we 5 0.09 NA 0.165 NA NA 0.5454545 0.4545455 0.4545455 1 
we 6 NaN NA 0.09 NA NA NA NA NaN 0 
we 7 0.09 NA 0.11875 NA NA 0.7578947 0.2421053 0.2421053 1 
we 8 0.09 NA 0.09714286 NA NA 0.9264706 0.07352941 0.07352941 1 

 weighted average (median_reduc_frac) : 0.1826075 ( 0 - 0.4545455 ) 
 weighted average (avg_reduc_frac) : 0.1826075 ( 0 - 0.4545455 ) 
 

Suburban stations:
WD/WE Cluster Avg_Ld sd_Ld Avg_noLd sd_noLd frac_RoA<1 RoA_median median_reduc_frac avg_reduc_frac n 
wd 1 NaN NA 0.09083333 0.001666667 NA NA NA NaN 0 
wd 2 0.09 0 0.1008333 0.02166667 NA 1 0 0.08125 4 
wd 3 0.09 0 0.09625 0.0125 NA 1 0 0.05434783 4 
wd 4 0.09 0 0.09083333 0.001666667 NA 1 0 0.01190476 3 
wd 5 0.09 0 0.09444444 0.008888889 NA 1 0 0.04123711 4 
wd 6 0.09 0 0.09535714 0.01071429 NA 1 0 0.04807692 4 
wd 7 0.09 0 0.09134615 0.002692308 NA 1 0 0.0141129 4 
wd 8 0.09275 0.003201562 0.09777778 0.01555556 67 1 0 0.03794597 4 

 weighted average (median_reduc_frac) : 0 ( 0 - 0 ) 
 weighted average (avg_reduc_frac) : 0.04235545 ( 0.01190476 - 0.08125 ) 
 
we 1 NaN NA 0.09 0 NA NA NA NaN 0 
we 2 0.09 NA 0.0925 0.005 NA 0.9 0.1 0.1 1 
we 3 NaN NA 0.09 0 NA NA NA NaN 0 
we 4 0.09 0 0.105 0.03 NA 1 0 0.1 4 
we 5 0.09 0 0.090625 0.00125 NA 1 0 0.006756757 4 
we 6 0.09 0 0.09 0 NA 1 0 0 3 
we 7 0.09 NA 0.09 0 NA 1 0 0 1 
we 8 0.09 0 0.09142857 0.002857143 NA 1 0 0.01492537 4 

 weighted average (median_reduc_frac) : 0.005882353 ( 0 - 0.1 ) 
 weighted average (avg_reduc_frac) : 0.03451344 ( 0 - 0.1 ) 
 

Rural stations:
WD/WE Cluster Avg_Ld sd_Ld Avg_noLd sd_noLd frac_RoA<1 RoA_median median_reduc_frac avg_reduc_frac n 
wd 1 NaN NA 0.0858 0.006942622 NA NA NA NaN 0 
wd 2 0.09 0 0.07266667 0.02385139 51 1 0 -0.3753846 5 
wd 3 0.09 0 0.08513333 0.007194133 51 1 0 -0.06365141 5 
wd 4 0.09 0 0.09 0 NA 1 0 0 2 
wd 5 0.09 0 0.08680952 0.005088781 51 1 0 -0.03978723 5 
wd 6 0.09 0 0.09 0 NA 1 0 0 5 
wd 7 0.09 0 0.08727273 0.006098367 76 1 0 -0.03571429 5 
wd 8 0.09 0 0.09 0 NA 1 0 0 5 

 weighted average (median_reduc_frac) : 0 ( 0 - 0 ) 
 weighted average (avg_reduc_frac) : -0.08039649 ( -0.3753846 - 0 ) 
 
we 1 NaN NA 0.09 0 NA NA NA NaN 0 
we 2 0.09 0 0.09 0 NA 1 0 0 3 
we 3 NaN NA 0.06375 0.03350995 NA NA NA NaN 0 
we 4 0.09 0 0.09 0 NA 1 0 0 5 
we 5 0.09 0 0.09 0 NA 1 0 0 5 
we 6 0.09 0 0.09 0 NA 1 0 0 2 
we 7 0.09 0 0.09 0 NA 1 0 0 3 
we 8 0.09 0 0.08766667 0.005217492 76 1 0 -0.02978723 5 

 weighted average (median_reduc_frac) : 0 ( 0 - 0 ) 
 weighted average (avg_reduc_frac) : -0.006475486 ( -0.02978723 - 0 ) 
 

