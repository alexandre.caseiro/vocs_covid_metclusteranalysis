
#ylab_rel_change <- expression(relative change==frac(lockdown - no lockdown,no lockdown))
ylab_rel_change <- bquote(paste('relative change = ', frac('lockdown - no lockdown','no lockdown')))

################################################################################

butane <- c(NA, 18, 20, -13, 9, 21, 34, -8)/100
pentane <- c(NA, 10, -4, -98, -10, 23, 8, -10)/100
hexane <- c(NA, 18, 16, -3, 17, 19, 12, 10)/100
SMP <- c(1:8)

png('plotsAnalysis/n-Alkanes.png', width=480, height=480*2/3)
par(mar=c(5,6,4,1))
plot(SMP, butane,
     ylim=c(min(butane, pentane, hexane, na.rm=TRUE), max(butane, pentane, hexane, na.rm=TRUE)),
     pch=0, cex=2, lwd=3,col="red",
     xlab='SMP', ylab=ylab_rel_change,
     main='n-Alkanes')
grid(col="grey35")
points(SMP, butane, pch=0, cex=2, lwd=3,col="red")
points(SMP, pentane, pch=1, cex=2, lwd=3, col="blue")
points(SMP, hexane, pch=2, cex=2, lwd=3, col="green")
legend("bottomright", c('Butane', 'Pentane', 'Hexane'),
       pch=c(0, 1, 2), col=c("red", "blue", "green"))
dev.off()

################################################################################

#aa <- c(90, 81, 67, NA, 51, 75, NA, 83)/100 # 1,2,3-Trimethylbenzene
bb <- c(NA, 0, 0, 0, 30, 30, 18, 0)/100 # 1,2,4-Trimethylbenzene
cc <- c(NA, 0, -9, 0, 3, 1, 0, 0)/100 # 1,3,5-Trimethylbenzene
dd <- c(NA, 17, 0, 1, 25, 14, 7, -2)/100 # Ethylbenzene
ee <- c(NA, 22, -1, 5, 33, 39, 15, -6)/100 # m,p-Xylene
ff <- c(NA, 0, 0, -9, 20, 8, 6, -1)/100 # o-Xylene
SMP <- c(1:8)

pch_tmb <- 0
pch_eth <- 3
pch_xyl <- 5

png('plotsAnalysis/ylbenzenes.png', width=480*1.25, height=480*2/3)
par(mar = c(5, 6, 4, 11.5), xpd=FALSE)
plot(SMP, bb,
     ylim=c(min(bb, cc, dd, ee, ff, na.rm=TRUE),
            max(bb, cc, dd, ee, ff, na.rm=TRUE)),
     pch=pch_tmb, cex=2, lwd=3,col="red",
     xlab='SMP', ylab=ylab_rel_change,
     main='Xylenes, Trimethylbenzenes and Ethylbenzene')
grid(col="grey35")
par(xpd=TRUE)
#points(SMP, aa, pch=pch_tmb, cex=2, lwd=3,col="red")
points(SMP, bb, pch=pch_tmb, cex=2, lwd=3, col="blue")
points(SMP, cc, pch=pch_tmb, cex=2, lwd=3, col="green")
points(SMP, dd, pch=pch_eth, cex=2, lwd=3, col="purple")
points(SMP, ee, pch=pch_xyl, cex=2, lwd=3, col="orange")
points(SMP, ff, pch=pch_xyl, cex=2, lwd=3, col="cyan")
legend("topright", inset=c(-0.425,0),
       #c('1,2,3-Trimethylbenzene', '1,2,4-Trimethylbenzene', '1,3,5-Trimethylbenzene',
       c('1,2,4-Trimethylbenzene', '1,3,5-Trimethylbenzene',
         'Ethylbenzene', 'm,p-Xylene', 'o-Xylene'),
       #pch=c(pch_tmb, pch_tmb, pch_tmb, pch_eth, pch_xyl, pch_xyl),
       pch=c(pch_tmb, pch_tmb, pch_eth, pch_xyl, pch_xyl),
       #col=c("red", "blue", "green", "purple", "orange", "cyan"))
       col=c("blue", "green", "purple", "orange", "cyan"))
dev.off()

################################################################################
