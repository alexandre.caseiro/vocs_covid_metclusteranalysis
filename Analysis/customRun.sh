source activate ClimPol04_gis

python 04_collateImagesH.py analysis_C6H6/l_C6H6__wd_meanCLoverBY.png analysis_C6H6/b_C6H6__wd_meanCLoverBY.png plotsAnalysis/lb_C6H6__wd.png

python 04_collateImagesH.py analysis_C6H6/b_C6H6_Traffic_wd_meanCLoverBY.png analysis_C6H6/l_C6H6_Traffic_wd_meanCLoverBY.png plotsAnalysis/lb_C6H6_Traffic_wd_meanCLoverBY.png
python 04_collateImagesH.py analysis_C6H6/b_C6H6_Industrial_wd_meanCLoverBY.png analysis_C6H6/l_C6H6_Industrial_wd_meanCLoverBY.png plotsAnalysis/lb_C6H6_Industrial_wd_meanCLoverBY.png
python 04_collateImagesH.py analysis_C6H6/b_C6H6_Background_wd_meanCLoverBY.png analysis_C6H6/l_C6H6_Background_wd_meanCLoverBY.png plotsAnalysis/lb_C6H6_Background_wd_meanCLoverBY.png
python 05_collateImagesV.py plotsAnalysis/lb_C6H6_Traffic_wd_meanCLoverBY.png plotsAnalysis/lb_C6H6_Industrial_wd_meanCLoverBY.png plotsAnalysis/lb_C6H6_Background_wd_meanCLoverBY.png plotsAnalysis/lb_C6H6_Type_wd_meansRatio.png
rm plotsAnalysis/lb_C6H6_Traffic_wd_meanCLoverBY.png plotsAnalysis/lb_C6H6_Industrial_wd_meanCLoverBY.png plotsAnalysis/lb_C6H6_Background_wd_meanCLoverBY.png

python 04_collateImagesH.py analysis_C6H6/b_C6H6_Urban_wd_meanCLoverBY.png analysis_C6H6/l_C6H6_Urban_wd_meanCLoverBY.png plotsAnalysis/lb_C6H6_Urban_wd_meanCLoverBY.png
python 04_collateImagesH.py analysis_C6H6/b_C6H6_Suburban_wd_meanCLoverBY.png analysis_C6H6/l_C6H6_Suburban_wd_meanCLoverBY.png plotsAnalysis/lb_C6H6_Suburban_wd_meanCLoverBY.png
python 04_collateImagesH.py analysis_C6H6/b_C6H6_Rural_wd_meanCLoverBY.png analysis_C6H6/l_C6H6_Rural_wd_meanCLoverBY.png plotsAnalysis/lb_C6H6_Rural_wd_meanCLoverBY.png
python 05_collateImagesV.py plotsAnalysis/lb_C6H6_Urban_wd_meanCLoverBY.png plotsAnalysis/lb_C6H6_Suburban_wd_meanCLoverBY.png plotsAnalysis/lb_C6H6_Rural_wd_meanCLoverBY.png plotsAnalysis/lb_C6H6_Area_wd_meansRatio.png
rm plotsAnalysis/lb_C6H6_Urban_wd_meanCLoverBY.png plotsAnalysis/lb_C6H6_Suburban_wd_meanCLoverBY.png plotsAnalysis/lb_C6H6_Rural_wd_meanCLoverBY.png

# C6H5-CH3

python 04_collateImagesH.py analysis_C6H5-CH3/b_C6H5-CH3__wd_meanCLoverBY.png analysis_C6H5-CH3/b_C6H5-CH3_Traffic_wd_meanCLoverBY.png plotsAnalysis/b_C6H5-CH3_All+Traffic_wd_meanCLoverBY.png
python 04_collateImagesH.py analysis_C6H5-CH3/b_C6H5-CH3_Background_wd_meanCLoverBY.png analysis_C6H5-CH3/b_C6H5-CH3_Suburban_wd_meanCLoverBY.png plotsAnalysis/b_C6H5-CH3_Bg+Sub_wd_meanCLoverBY.png
python 05_collateImagesV.py plotsAnalysis/b_C6H5-CH3_All+Traffic_wd_meanCLoverBY.png plotsAnalysis/b_C6H5-CH3_Bg+Sub_wd_meanCLoverBY.png plotsAnalysis/b_C6H5-CH3_All+Traffic+Bg+Sub_wd_meanCLoverBY.png
rm plotsAnalysis/b_C6H5-CH3_All+Traffic_wd_meanCLoverBY.png plotsAnalysis/b_C6H5-CH3_Bg+Sub_wd_meanCLoverBY.png


# xylenes

python 04_collateImagesH.py analysis_o-C6H4-\(CH3\)2/b_o-C6H4-\(CH3\)2__wd_meanCLoverBY.png analysis_m\,p-C6H4\(CH3\)2/b_m\,p-C6H4\(CH3\)2__wd_meanCLoverBY.png plotsAnalysis/b_Xylenes__wd_meanCLoverBY.png

python 04_collateImagesH.py analysis_o-C6H4-\(CH3\)2/b_o-C6H4-\(CH3\)2_Background_wd_meanCLoverBY.png analysis_m\,p-C6H4\(CH3\)2/b_m\,p-C6H4\(CH3\)2_Background_wd_meanCLoverBY.png plotsAnalysis/b_Xylenes_Background_wd_meanCLoverBY.png

python 04_collateImagesH.py analysis_o-C6H4-\(CH3\)2/b_o-C6H4-\(CH3\)2_Urban_wd_meanCLoverBY.png analysis_m\,p-C6H4\(CH3\)2/b_m\,p-C6H4\(CH3\)2_Urban_wd_meanCLoverBY.png plotsAnalysis/b_Xylenes_Urban_wd_meanCLoverBY.png
python 04_collateImagesH.py analysis_o-C6H4-\(CH3\)2/b_o-C6H4-\(CH3\)2_Suburban_wd_meanCLoverBY.png analysis_m\,p-C6H4\(CH3\)2/b_m\,p-C6H4\(CH3\)2_Suburban_wd_meanCLoverBY.png plotsAnalysis/b_Xylenes_Suburban_wd_meanCLoverBY.png
python 05_collateImagesV.py plotsAnalysis/b_Xylenes_Urban_wd_meanCLoverBY.png plotsAnalysis/b_Xylenes_Suburban_wd_meanCLoverBY.png plotsAnalysis/b_Xylenes_Area_wd_meanCLoverBY.png
rm plotsAnalysis/b_Xylenes_Urban_wd_meanCLoverBY.png plotsAnalysis/b_Xylenes_Suburban_wd_meanCLoverBY.png


# Trimethylbenzenes

python 04_collateImagesH.py analysis_1\,2\,4-C6H3\(CH3\)3//b_1\,2\,4-C6H3\(CH3\)3__wd_meanCLoverBY.png analysis_1\,3\,5-C6H3\(CH3\)3//b_1\,3\,5-C6H3\(CH3\)3__wd_meanCLoverBY.png plotsAnalysis/b_Trimethylbenzenes__wd_meanCLoverBY.png

# b:t:e

python 04_collateImagesH.py analysis_BTE/BTE_SMP_diff3.png analysis_BTE/BTE_SMP_diff4.png analysis_BTE/BTE_SMP_diff5.png analysis_BTE/BTE_by_SMPup.png
python 04_collateImagesH.py analysis_BTE/BTE_SMP_diff6.png analysis_BTE/BTE_SMP_diff7.png analysis_BTE/BTE_SMP_diff8.png analysis_BTE/BTE_by_SMPdn.png
python 05_collateImagesV.py analysis_BTE/BTE_by_SMPup.png analysis_BTE/BTE_by_SMPdn.png analysis_BTE/BTE_by_SMP.png
rm analysis_BTE/BTE_by_SMPup.png analysis_BTE/BTE_by_SMPdn.png
