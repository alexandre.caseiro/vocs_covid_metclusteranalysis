
#mkdir plotsAnalysis/
#mkdir PolsMaps/

source activate ClimPol04_gis

# make the plot of the distribution of the clusters throughout the years
#Rscript s_001_mkClustersDistribPlot.R
# output: the file plotsAnalysis/clustersDistrib.png 

# a file with important variables: s_005_variables.R
# produced by hand
# the file s_005_variables.R will be used throughout the project

###############################################################################
# select the data
###############################################################################

#Rscript s_006_dataIN.R
# outputs:
# (1) i_001_dataIN.RData, with the tables:
# dAll: all the data from Airbase
# dAll_DoI: same as above, subset for the dates of interest
# dAll_DoI_TR: same as above, subset for the time resolution. Station/pollutant pairs with only hourly or daily data: keep the original, station/pollutant pairs with both hourly and daily data: make daily averages.
# dAll_DoI_TR_DC: same as above, subset for data coverage (only station/pollutant pairs above the threshold (see s_005_variables.R for the threshold and if it is used or not) are kept).
# dAll_DoI_TR_DC_wRatios: same as above, but with the Toluene/Benzene ratio and the m,p-Xylene/Ethylbenzene ratio
# dAirbase: same as above, but only with the Airbase data and without the satellite data (Pollutants HCHOmean and HCHOmax)
# dAirbase_bte: the B:T:E ratios
# (2) files about the data
# dataDS.txt: notes about the evoluation of the data set, from the downloaded data to the final data set for analysis.
# dataDC.txt and dataDC.tex: about the data coverage of the data.
# dataTR.txt and dataTR.tex

# collate data coverage
#python 04_collateImagesH.py PolsPlots/DC_H3C-CH2-CH3.png PolsPlots/DC_C6H6.png PolsPlots/DC_Propane_Benzene.png
#python 04_collateImagesH.py PolsPlots/DC_C6H5-CH3.png PolsPlots/DC_m\,p-C6H4\(CH3\)2.png PolsPlots/DC_Toluene_Xylene.png
#python 05_collateImagesV.py PolsPlots/DC_Propane_Benzene.png PolsPlots/DC_Toluene_Xylene.png PolsPlots/DC_4species.png

###############################################################################
# analyse the concentrations data,
# at the level of station and country and pollutant
###############################################################################

#Rscript s_010_mkStationAnalysis.R
# outputs:
# (1) boxplots and histograms for each pollutant, wd and we of the ratios of means, ratios of medians and p-values
# at the station, country and continental levels. PolsPlots/ directory
# (2) ASCII file for each pollutant, each record refers to a single station and a daytype (wd or we),
# the fields are: country, pollutant, station, type, area, cluster, wdwe, p-value, nBY, nCL, meanCLoverBY, medianCLoverBY, conc_meanCL, conc_meanBY, conc_medianCL,conc_medianBY

#Rscript s_020_mkPolAnalysis.R
# outputs:
# for each pollutant, within a folder (analysis_Pollutant):
# (1) a text file Analysis_Pollutant.txt with information on means, medians, p-values, ratios, ... differentiated by station type, area, wd/we.
# (2) boxplots and lineplots for means, medians, p-values, ratios, ... differentiated by station type, area, wd/we.

###############################################################################
# analyse the ratios
###############################################################################

#Rscript s_030_mkRatioAnalysis.R
# outputs:
# for C6H5-CH3--OVER--C6H6 and m,p-C6H4(CH3)2--OVER--C6H5-C2H5: boxplots for wd and we (all data, "AllEU", 2 files each)
# for C6H5-CH3--OVER--C6H6: a scatter plot

#Rscript s_036_mkBTEanalysis.R
# outputs:
# in folder analysis_BTE, plots and tables about BTE

###############################################################################
# analyse the correlations
###############################################################################

#Rscript s_037_mkCorrelAnalysis.R
# outputs:
# in folder analysis_correl, 

#Rscript s_038_mkCorrelPlots.R
# outputs:
# 

###############################################################################
# make maps
###############################################################################

#Rscript s_041_mapBenzene.R
#python 04_collateImagesH.py PolsMaps/map_benzene_mape.png PolsMaps/map_benzene_cbar.png PolsMaps/map_benzene.png
#Rscript s_042_mapToluene.R
#python 04_collateImagesH.py PolsMaps/map_toluene_mape.png PolsMaps/map_toluene_cbar.png PolsMaps/map_toluene.png
#Rscript s_043_mapmpXylene.R
#python 04_collateImagesH.py PolsMaps/map_mpxylene_mape.png PolsMaps/map_mpxylene_cbar.png PolsMaps/map_mpxylene.png
#Rscript s_044_mapoxylene.R
#python 04_collateImagesH.py PolsMaps/map_oxylene_mape.png PolsMaps/map_oxylene_cbar.png PolsMaps/map_oxylene.png

###############################################################################
# collate some plots
###############################################################################

# C6H6

#python 04_collateImagesH.py analysis_C6H6/l_C6H6__wd_meanCLoverBY.png analysis_C6H6/b_C6H6__wd_meanCLoverBY.png plotsAnalysis/lb_C6H6__wd.png

#python 04_collateImagesH.py analysis_C6H6/b_C6H6_Traffic_wd_meanCLoverBY.png analysis_C6H6/l_C6H6_Traffic_wd_meanCLoverBY.png plotsAnalysis/lb_C6H6_Traffic_wd_meanCLoverBY.png
#python 04_collateImagesH.py analysis_C6H6/b_C6H6_Industrial_wd_meanCLoverBY.png analysis_C6H6/l_C6H6_Industrial_wd_meanCLoverBY.png plotsAnalysis/lb_C6H6_Industrial_wd_meanCLoverBY.png
#python 04_collateImagesH.py analysis_C6H6/b_C6H6_Background_wd_meanCLoverBY.png analysis_C6H6/l_C6H6_Background_wd_meanCLoverBY.png plotsAnalysis/lb_C6H6_Background_wd_meanCLoverBY.png
#python 05_collateImagesV.py plotsAnalysis/lb_C6H6_Traffic_wd_meanCLoverBY.png plotsAnalysis/lb_C6H6_Industrial_wd_meanCLoverBY.png plotsAnalysis/lb_C6H6_Background_wd_meanCLoverBY.png plotsAnalysis/lb_C6H6_Type_wd_meansRatio.png
#rm plotsAnalysis/lb_C6H6_Traffic_wd_meanCLoverBY.png plotsAnalysis/lb_C6H6_Industrial_wd_meanCLoverBY.png plotsAnalysis/lb_C6H6_Background_wd_meanCLoverBY.png

#python 04_collateImagesH.py analysis_C6H6/b_C6H6_Urban_wd_meanCLoverBY.png analysis_C6H6/l_C6H6_Urban_wd_meanCLoverBY.png plotsAnalysis/lb_C6H6_Urban_wd_meanCLoverBY.png
#python 04_collateImagesH.py analysis_C6H6/b_C6H6_Suburban_wd_meanCLoverBY.png analysis_C6H6/l_C6H6_Suburban_wd_meanCLoverBY.png plotsAnalysis/lb_C6H6_Suburban_wd_meanCLoverBY.png
#python 04_collateImagesH.py analysis_C6H6/b_C6H6_Rural_wd_meanCLoverBY.png analysis_C6H6/l_C6H6_Rural_wd_meanCLoverBY.png plotsAnalysis/lb_C6H6_Rural_wd_meanCLoverBY.png
#python 05_collateImagesV.py plotsAnalysis/lb_C6H6_Urban_wd_meanCLoverBY.png plotsAnalysis/lb_C6H6_Suburban_wd_meanCLoverBY.png plotsAnalysis/lb_C6H6_Rural_wd_meanCLoverBY.png plotsAnalysis/lb_C6H6_Area_wd_meansRatio.png
#rm plotsAnalysis/lb_C6H6_Urban_wd_meanCLoverBY.png plotsAnalysis/lb_C6H6_Suburban_wd_meanCLoverBY.png plotsAnalysis/lb_C6H6_Rural_wd_meanCLoverBY.png

# C6H5-CH3

#python 04_collateImagesH.py analysis_C6H5-CH3/b_C6H5-CH3__wd_meanCLoverBY.png analysis_C6H5-CH3/b_C6H5-CH3_Traffic_wd_meanCLoverBY.png plotsAnalysis/b_C6H5-CH3_All+Traffic_wd_meanCLoverBY.png
#python 04_collateImagesH.py analysis_C6H5-CH3/b_C6H5-CH3_Background_wd_meanCLoverBY.png analysis_C6H5-CH3/b_C6H5-CH3_Suburban_wd_meanCLoverBY.png plotsAnalysis/b_C6H5-CH3_Bg+Sub_wd_meanCLoverBY.png
#python 05_collateImagesV.py plotsAnalysis/b_C6H5-CH3_All+Traffic_wd_meanCLoverBY.png plotsAnalysis/b_C6H5-CH3_Bg+Sub_wd_meanCLoverBY.png plotsAnalysis/b_C6H5-CH3_All+Traffic+Bg+Sub_wd_meanCLoverBY.png
#rm plotsAnalysis/b_C6H5-CH3_All+Traffic_wd_meanCLoverBY.png plotsAnalysis/b_C6H5-CH3_Bg+Sub_wd_meanCLoverBY.png


# xylenes

#python 04_collateImagesH.py analysis_o-C6H4-\(CH3\)2/b_o-C6H4-\(CH3\)2__wd_meanCLoverBY.png analysis_m\,p-C6H4\(CH3\)2/b_m\,p-C6H4\(CH3\)2__wd_meanCLoverBY.png plotsAnalysis/b_Xylenes__wd_meanCLoverBY.png

#python 04_collateImagesH.py analysis_o-C6H4-\(CH3\)2/b_o-C6H4-\(CH3\)2_Background_wd_meanCLoverBY.png analysis_m\,p-C6H4\(CH3\)2/b_m\,p-C6H4\(CH3\)2_Background_wd_meanCLoverBY.png plotsAnalysis/b_Xylenes_Background_wd_meanCLoverBY.png

#python 04_collateImagesH.py analysis_o-C6H4-\(CH3\)2/b_o-C6H4-\(CH3\)2_Urban_wd_meanCLoverBY.png analysis_m\,p-C6H4\(CH3\)2/b_m\,p-C6H4\(CH3\)2_Urban_wd_meanCLoverBY.png plotsAnalysis/b_Xylenes_Urban_wd_meanCLoverBY.png
#python 04_collateImagesH.py analysis_o-C6H4-\(CH3\)2/b_o-C6H4-\(CH3\)2_Suburban_wd_meanCLoverBY.png analysis_m\,p-C6H4\(CH3\)2/b_m\,p-C6H4\(CH3\)2_Suburban_wd_meanCLoverBY.png plotsAnalysis/b_Xylenes_Suburban_wd_meanCLoverBY.png
#python 05_collateImagesV.py plotsAnalysis/b_Xylenes_Urban_wd_meanCLoverBY.png plotsAnalysis/b_Xylenes_Suburban_wd_meanCLoverBY.png plotsAnalysis/b_Xylenes_Area_wd_meanCLoverBY.png
#rm plotsAnalysis/b_Xylenes_Urban_wd_meanCLoverBY.png plotsAnalysis/b_Xylenes_Suburban_wd_meanCLoverBY.png


# Trimethylbenzenes

#python 04_collateImagesH.py analysis_1\,2\,4-C6H3\(CH3\)3//b_1\,2\,4-C6H3\(CH3\)3__wd_meanCLoverBY.png analysis_1\,3\,5-C6H3\(CH3\)3//b_1\,3\,5-C6H3\(CH3\)3__wd_meanCLoverBY.png plotsAnalysis/b_Trimethylbenzenes__wd_meanCLoverBY.png

# b:t:e

#python 04_collateImagesH.py analysis_BTE/BTE_SMP_diff3.png analysis_BTE/BTE_SMP_diff4.png analysis_BTE/BTE_SMP_diff5.png analysis_BTE/BTE_by_SMPup.png
#python 04_collateImagesH.py analysis_BTE/BTE_SMP_diff6.png analysis_BTE/BTE_SMP_diff7.png analysis_BTE/BTE_SMP_diff8.png analysis_BTE/BTE_by_SMPdn.png
#python 05_collateImagesV.py analysis_BTE/BTE_by_SMPup.png analysis_BTE/BTE_by_SMPdn.png analysis_BTE/BTE_by_SMP.png

#rm analysis_BTE/BTE_by_SMPup.png analysis_BTE/BTE_by_SMPdn.png
###############################################################################


