library(maps)
library(mapdata)
library(RColorBrewer)

source('f_analysis.R')

whichLevel <- function(x) {
  levs <- c(0, seq(0.375, 0.999, 0.125), 1, 1.25, 1.5, 2, 2.5, 3)
  return(which((x-levs)==max(x-levs[x-levs<=0])))
}

tolueneFile <- 'PolsData/C6H5-CH3.csv'

stationMetadataFile <- '../Airbase/PanEuropean_metadata.csv'

tolu <- read.table(tolueneFile, sep=';', header=TRUE)

the_palette <- brewer.pal(n=11, name="Spectral")

sta <- read.table(stationMetadataFile, sep="\t", header=TRUE)
# subset for toluene only (AirPollutantcode is 21
# https://dd.eionet.europa.eu/vocabulary/aq/pollutant)
sta <- subset(sta, AirPollutantCode=='http://dd.eionet.europa.eu/vocabulary/aq/pollutant/21')

c_sta <- c()
c_fra <- c()
c_lon <- c()
c_lat <- c()
c_type <- c()
c_area <- c()

# get the ratio lockdown/no lockdown, per station
# and get station lat lon
for (station in unique(tolu$c_station)) {
  d <- subset(tolu, c_station==station)
  mean_concCL <- sum(d$c_nCL * d$c_conc_meanCL, na.rm=TRUE)/sum(d$c_nCL, na.rm=TRUE)
  mean_concBY <- sum(d$c_nBY * d$c_conc_meanBY, na.rm=TRUE)/sum(d$c_nBY, na.rm=TRUE)
  c_sta <- c(c_sta, station)
  c_fra <- c(c_fra, mean_concCL/mean_concBY)
  c_lon <- c(c_lon, mean(sta[sta$AirQualityStation==station,]$Longitude))
  c_lat <- c(c_lat, mean(sta[sta$AirQualityStation==station,]$Latitude))
  c_type <- c(c_type, unique(tolu[tolu$c_station==station,]$c_type))
  c_area <- c(c_area, unique(tolu[tolu$c_station==station,]$c_area))
}

# build a data frame and assign a color, a station type and a station area
dd <- data.frame(
  'station' = c_sta,
  'ratio' = c_fra,
  'lon' = c_lon,
  'lat' = c_lat,
  'type' = c_type,
  'area' = c_area
)
# get rid of Inf and negative values in the ratio
ddd <- dd[is.finite(dd$ratio) & dd$ratio>0,]

# define the levels
ddd$level <- unlist(lapply(ddd$ratio, FUN=whichLevel))
# level 2 should be for [0:0.375], but there is no data with level==1
ddd$level <- ddd$level-1
# set the colors
ddd$color <- the_palette[ddd$level]

png('PolsMaps/map_toluene_mape.png', width=480, height=480)

map('worldHires', xlim=c(-10,38), ylim=c(36,71), fill=TRUE, col="grey60", border="grey60", mar=rep(0,4))
# plot by type
points(subset(ddd, type=='traffic')$lon, subset(ddd, type=='traffic')$lat, col=subset(ddd, type=='traffic')$color, pch=0, cex=0.75, lwd=2)
points(subset(ddd, type=='background')$lon, subset(ddd, type=='background')$lat, col=subset(ddd, type=='background')$color, pch=1, lwd=2)
points(subset(ddd, type=='industrial')$lon, subset(ddd, type=='industrial')$lat, col=subset(ddd, type=='industrial')$color, pch=2, lwd=2)
# title on the right
mtext('Toluene', cex=2, side=4, line=1)
# the legend
legend("topleft", c('Traffic', 'Background', 'Industrial'), pch=c(0, 1, 2), bty = "n")

dev.off()

cols_cbar <- c(the_palette[1], the_palette[1], the_palette[1], # ]0:0.375]
               the_palette[2], the_palette[3], the_palette[4], # ]0.375:0.750]
               the_palette[5], the_palette[6], # ]0.750:1]
               the_palette[7], the_palette[7], # ]1:1.25]
               the_palette[8], the_palette[8], # ]1.25:1.5]
               rep(the_palette[9], 4), # ]1.5:2]
               rep(the_palette[10], 4), # ]2:2.5]
               rep(the_palette[11], 4) # ]2.5:3]
               )
min_cbar <- 0
max_cbar <- 3
scale_cbar <- 0.125
nLevels_cbar <- 3/scale_cbar+1
seq_starts <- seq(min_cbar, max_cbar-scale_cbar, scale_cbar)

png('PolsMaps/map_toluene_cbar.png', width=480/5, height=480)

par(oma=c(0,0,0,0), mar=c(0,4,0,0))

plot(c(0,10), c(min_cbar, max_cbar),
     type='n', bty='n', xaxt='n', xlab='', yaxt='n', ylab='', main='')
axis(2, seq(min_cbar, max_cbar, len=nLevels_cbar), las=1, line=-1.5)
for (y in seq_starts) {
 rect(6,y,10,y+scale_cbar, col=cols_cbar[which(seq_starts==y)], border=NA)
}
mtext('Lockdown / no Lockdown', side=2, line=2.25)

dev.off()
