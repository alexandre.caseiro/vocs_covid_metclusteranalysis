###############################################################################
dirAirbase_2015_2019 <- '/worknb/projects/aqmh/Airbase/downloaded_for_VOCs_covid/2021-08-02__downloaded/*'
dirAirbase_2020_2020 <- '/worknb/projects/aqmh/Airbase/downloaded_for_VOCs_covid/2022-04-29__downloaded/*'
# Both directories above contain only E1a data
countriesFile <- 'countriesToStudy.txt'
###############################################################################

# read the downloaded Airbase files
allAirbaseFiles <- c(Sys.glob(dirAirbase_2015_2019), Sys.glob(dirAirbase_2020_2020))

# read the countries file
countriesToStudy <- read.table(countriesFile, header=TRUE, sep=';')

countries <- subset(countriesToStudy, !is.na(startLD) & !is.na(endLD))$country

for (country in countries) {

# subset the downloaded Airbase files
AirbaseFiles <- allAirbaseFiles[grep(paste('/',country,'_', sep=''), allAirbaseFiles)]

# create a data frame
lt <- data.frame(
  countryCode = sapply(strsplit(sapply(strsplit(AirbaseFiles, '_'), "[[", 6), '/'), "[[", 2),
  pollutantCode = sapply(strsplit(AirbaseFiles, '_'), "[[", 7),
  stationCode = sapply(strsplit(AirbaseFiles, '_'), "[[", 8),
  year = sapply(strsplit(AirbaseFiles, '_'), "[[", 9)
  )
# we're interested in the years [2016-2020]
lt <- subset(lt, year>2015)

# aggregate the data
la <- aggregate(as.numeric(pollutantCode) ~ countryCode + stationCode + pollutantCode, data=lt, FUN=sum)
# keep the combinations country/station/pollutant where there are at least 5 years (this means the sum of the pollutantCode in the aggregated data is 5 times the pollutantCode)
colnames(la) <- c(colnames(la)[1:length(colnames(la))-1], 'sum_pollutantCode')
ls <- subset(la, sum_pollutantCode/as.numeric(pollutantCode)==5)

ls$fileNamepattern <- paste(ls$countryCode, ls$pollutantCode, ls$stationCode, sep='_')

matches <- unique(grep(paste(ls$fileNamepattern, collapse="|"), AirbaseFiles, value=TRUE))
write.table(matches, paste('Airbase_List_',country,'_2016_2020.txt', sep=''),
                           row.names=FALSE, col.names=FALSE, quote=FALSE, append=FALSE)

}
